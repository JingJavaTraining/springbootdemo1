package com.allstate.dao;

import com.allstate.entities.Employee;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Repository
@Qualifier("mysql")
public class EmployeeImplMySql implements EmployeeRepo{

    public void cleardb()
    {
        try {
            PreparedStatement preparedStatement=this.connection.prepareStatement("Truncate Table Employees");

            preparedStatement.execute();


        } catch (SQLException throwables) {
            throwables.printStackTrace();

        }

    }
    private Connection getConnection() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            this.connection= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/training?serverTimezone=UTC&useSSL=false","root","c0nygre");
            //application.properties

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return this.connection;
    }

   public EmployeeImplMySql() {

   getConnection();
   }

    private Connection connection;

    public boolean isConnected(){

        Statement statement = null;
        try {

            statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery("select 1");
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }



    }

    @Override
    public int create(Employee employee) {

        try {
            PreparedStatement preparedStatement=this.connection.prepareStatement("Insert into Employees(name, address, salary) values(?,?,?)");

            preparedStatement.setString(1, employee.getName());
            preparedStatement.setString(2, employee.getAddress());
            preparedStatement.setDouble(3, employee.getSalary());

            return preparedStatement.executeUpdate();



        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }



    }


    @Override
    public int update(Employee employee) {

        try {
            PreparedStatement preparedStatement=this.connection.prepareStatement("Update Employees set name=?, address=?,salary=? where id=?");

            preparedStatement.setString(1, employee.getName());
            preparedStatement.setString(2, employee.getAddress());
            preparedStatement.setDouble(3, employee.getSalary());
            preparedStatement.setInt(4, employee.getId());

            return preparedStatement.executeUpdate();



        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }



    }

    @Override
    public Employee findById(int id) {
        try {
            PreparedStatement preparedStatement=this.connection.prepareStatement("Select * From Employees where id=?");

            preparedStatement.setInt(1, id);

            ResultSet rs= preparedStatement.executeQuery();
            rs.first();
            Employee employee = new Employee(rs.getString("name"),
                                    rs.getInt("id"),
                                    rs.getDouble("salary"),
                                    rs.getString("address")
                    );

            return employee;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }





    }

    @Override
    public List<Employee> findAll() {

        List<Employee> employees = new ArrayList<>();
        try {
            PreparedStatement preparedStatement=this.connection.prepareStatement("Select * From Employees");


            ResultSet rs= preparedStatement.executeQuery();

            while(rs.next())
            {
                Employee employee = new Employee(rs.getString("name"),
                        rs.getInt("id"),
                        rs.getDouble("salary"),
                        rs.getString("address")
                );
                employees.add(employee);

            }

            return employees;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }



    }

    @Override
    public List<Employee> findByAddress(String address) {

        List<Employee> employees = new ArrayList<>();
        try {
            PreparedStatement preparedStatement=this.connection.prepareStatement("Select * From Employees Where address=?");

            preparedStatement.setString(1, address );

            ResultSet rs= preparedStatement.executeQuery();

            while(rs.next())
            {
                Employee employee = new Employee(rs.getString("name"),
                        rs.getInt("id"),
                        rs.getDouble("salary"),
                        rs.getString("address")
                );
                employees.add(employee);

            }

            return employees;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }


    }
}
