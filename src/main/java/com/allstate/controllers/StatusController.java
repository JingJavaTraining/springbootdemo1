package com.allstate.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/status")
public class StatusController {

   @RequestMapping(value="/getstatus",method = RequestMethod.GET)

        public String getStatus()
        {
            return "Api is running";
        }

}
