package com.allstate.controllers;

import com.allstate.entities.Employee;
import com.allstate.exceptions.OutOfRangeException;
import com.allstate.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value="/find/{id}", method= RequestMethod.GET)
    public Employee findById(@PathVariable int id) throws OutOfRangeException {
        return employeeService.find(id);



    }



    @RequestMapping(value="/findall", method= RequestMethod.GET)
    public List<Employee>findAll() throws OutOfRangeException {
        return employeeService.findAll();



    }


    @RequestMapping(value="/save", method= RequestMethod.POST)
    public int save(@RequestBody Employee employee) throws OutOfRangeException {
        return employeeService.save(employee);



    }

}
