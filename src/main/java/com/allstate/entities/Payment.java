package com.allstate.entities;

import java.util.Date;

public class Payment {

    private int id;
    private String custId;
    private double amt;
    private Date date;

    public Payment(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", custId='" + custId + '\'' +
                ", amt=" + amt +
                ", date=" + date +
                '}';
    }

    public Payment(int id, String custId) {


        this.id = id;
        this.custId = custId;
    }


    public Payment(int id, String custId, double amt) {
        this.id = id;
        this.custId = custId;
        this.amt = amt;
    }

    public Payment(int id, String custId, double amt, Date date) {
        this.id = id;
        this.custId = custId;
        this.amt = amt;
        this.date = date;
    }

    public Payment() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
