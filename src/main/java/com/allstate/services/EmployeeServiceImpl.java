package com.allstate.services;

import com.allstate.dao.EmployeeRepo;
import com.allstate.entities.Employee;
import com.allstate.exceptions.OutOfRangeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
//buisness layer
public class EmployeeServiceImpl implements  EmployeeService{

    @Autowired
    @Qualifier("mongo")
    private EmployeeRepo employeeRepo;

    public EmployeeServiceImpl()
    {

    }
    public EmployeeServiceImpl(EmployeeRepo employeeRepo)
    {

        this.employeeRepo=employeeRepo;

    }

    @Override
    public Employee find(int id) throws OutOfRangeException {
        if(id<1){
            throw new OutOfRangeException("id must be over 0", 1);
        }
        return employeeRepo.findById(id);
    }

    @Override
    public List<Employee> find(String address) throws OutOfRangeException {
        if(address.equals("")){
            throw new OutOfRangeException("Address can'tbe blank", 1);
        }
        return employeeRepo.findByAddress(address);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepo.findAll();
    }

    @Override
    public int save(Employee employee) throws OutOfRangeException {
        if(employee.getId()<1){
            throw new OutOfRangeException("ID must be over 1", 1);
        }

        Employee e1 = employeeRepo.findById(employee.getId());
        if(e1 == null)
        {
            return employeeRepo.create(employee);
        }
        else
            return -2;


    }

    @Override
    public int update(Employee employee) throws OutOfRangeException {
        if(employee.getId()<1){
            throw new OutOfRangeException("id must be over 0", 1);
        }
        return employeeRepo.update(employee);


    }
}
